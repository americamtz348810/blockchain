# Práctica | Creación de un blockchain con nodejs

Con nuestros conocimientos adquiridos de Javascript y  de Node.Js programamos un blockchain

## Blockchain

Como su nombre lo indica, ‘blockchain’ es una cadena de bloques, los cuales contienen información codificada de una transacción en la red. Y, al estar entrelazados (de ahí la palabra cadena), permiten la transferencia de datos (o valor) con una codificación bastante segura a través del uso de criptografía. Para ilustrar esta idea, sería conveniente imaginarnos un libro contable en donde se registran todas las entradas y salidas de dinero.

Lo verdaderamente novedoso es que la transferencia no exige un tercero que certifique la información, sino que está distribuida en múltiples nodos independientes e iguales entre sí que la examinan y la validan sin necesidad de que se conozcan entre ellos. Una vez introducida, la información no puede ser eliminada, sólo se podrá añadir nueva información, ya que los bloques están conectados entre sí a través de cifrado criptográfico, por lo que modificar datos de un bloque anterior a la cadena resulta imposible, ya que se tendría que modificar la información de los bloques anteriores.

### Pre-requisitos
- Tener instalado nodejs

### Instalación

Para instalar nodejs:

```
sudo apt update
sudo apt install nodejs
```

Para comprobar que se ha instalado correctamente:

```
node --v
```
Si se ha instalado correctamente nos aparecerá la versión instalada

## Ejecutando

Para ejecutar nuestra práctica "blockchain" usaremos el siguiente comando:

```
node main.js
```

### Analice

Una vez ejecutado nos deberá arrojar los bloques según la dificultad.

## Construido con

Menciona las herramientas que utilizaste para crear tu proyecto

* [Javascript] - Lenguaje
* [Visual Studio Code] - Editor de texto
* [Node.js] - Entorno de ejecución

## Autor

* **América Martínez** - *348810* - (https://gitlab.com/americamtz348810)

## Información

* ** Docente: Ing. Luis Antonio Ramírez Martínez - Desarrollo Basado en Plataformas
* ** Universidad Autónoma de Chihuahua - 6CC2
